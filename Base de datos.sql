/*Gestion notas appp android*/


CREATE TABLE semestre(

	num_semestre int(2),
	anio int(4),
	PRIMARY KEY(num_semestre)

);


CREATE TABLE asignatura(

	codigo VARCHAR(8),
	nombre_asig VARCHAR(30),
	creditos INT(1),
	num_semestre INT(2),
	dia_asistencia VARCHAR(8),
	hora_entrada VARCHAR(4),
	hora_salida VARCHAR(4),
	PRIMARY KEY(codigo),
	CONSTRAINT num_semestre_fk FOREIGN KEY(num_semestre) REFERENCES semestre(num_semestre)

);


CREATE TABLE nota(

	nom_general VARCHAR(10),
	porcentaje INT(2),
	calificacion DECIMAL(4,2),
	codigo VARCHAR(8),
	PRIMARY KEY(nom_general),
	CONSTRAINT codigo_fk FOREIGN KEY(codigo) REFERENCES asignatura(codigo)

);


CREATE TABLE notas(

	nombre VARCHAR(10),
	calificacion DECIMAL(4,2),
	nom_general VARCHAR(10),
	PRIMARY KEY(nombre),
	CONSTRAINT nombre_fk FOREIGN KEY(nom_general) REFERENCES nota(nom_general)

);