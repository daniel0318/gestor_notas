package com.example.dani_.gestor_notas;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

public class Lista_asignatura extends AppCompatActivity {

    private FloatingActionButton boton;
    private ListView lista;
    private Base_de_datos base_de_datos;
    private ArrayList<Asignatura> asignaturas;
    private Adaptador_asignaturas adaptador_asignaturas;
    private Semestre semestre;
    private Asignatura asignatura;/***/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_asignatura);
        instanciar_objetos();
        poner_oidos();
        llenar_lista();
    }

    public void llenar_lista(){
        semestre = (Semestre) getIntent().getExtras().getSerializable("semestre");
        Toast.makeText(getApplicationContext(),"me llego" + semestre.getSemestre() + " " + semestre.getAño(),Toast.LENGTH_LONG).show();
        boolean prueba = false;
        int tamaño = 0;
        try {
            asignaturas = base_de_datos.leer_asignaturas_semestre(semestre);
            prueba = asignaturas.isEmpty();
            tamaño = asignaturas.size();
            Toast.makeText(getApplication(),asignaturas.get(0).getNombre(),Toast.LENGTH_SHORT).show();


            if(!asignaturas.isEmpty()) {
                adaptador_asignaturas = new Adaptador_asignaturas(getApplicationContext(),R.layout.view_asignaturas);
                lista.setAdapter(adaptador_asignaturas);
                for (int i=0;i<asignaturas.size();i++){/**Meno o menor o igual?*/
                    adaptador_asignaturas.add(asignaturas.get(i));
                }
            }
        }catch (Exception e){
           Toast.makeText(getApplicationContext(), "Ocurrio un error en llenar_lista " + prueba + " "+ tamaño, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    public void instanciar_objetos(){
        asignaturas = new ArrayList<Asignatura>();
        boton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        lista = (ListView) findViewById(R.id.listview_asignaturas);
        lista.setEmptyView(findViewById(R.id.empty2));
        base_de_datos = new Base_de_datos(this);
        /**Asociamos el menu contextual*/
        registerForContextMenu(lista);
    }

    public void poner_oidos(){
        boton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AgregarAsignatura.class);
                i.putExtra("semestre", semestre);
                startActivity(i);
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Asignatura asignatura = asignaturas.get(position);
                Intent intent = new Intent(getApplicationContext(), Consultar_nota.class);
                intent.putExtra("asignatura",asignatura);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(Menu.NONE,1, Menu.NONE,"Editar");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE,2,Menu.NONE,"Eliminar").setHeaderTitle("Confirmar eliminacion");
        subMenu.add(Menu.NONE,21, Menu.NONE,"SI");
        subMenu.add(Menu.NONE,22, Menu.NONE,"NO");

        super.onCreateContextMenu(menu, v, menuInfo);


        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        asignatura = (Asignatura) lista.getAdapter().getItem(info.position);
        menu.setHeaderTitle(asignatura.getNombre());

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getItemId() == 1){
            Intent i = new Intent(getApplicationContext(), AgregarAsignatura.class);
            i.putExtra("asignatura", asignatura);
            i.putExtra("semestre", semestre);
            i.putExtra("Editar", "editando");/**Se busca en el intent esa frase para asi saber que se esta haciendo*/
            startActivity(i);
        }
        else if(item.getItemId() == 21){
            Toast.makeText(getApplicationContext(),"NO SE QUE HACER :v",Toast.LENGTH_SHORT).show();
        }
        else if(item.getItemId() == 22){
            Toast.makeText(getApplicationContext(),"Se a cancelado la eliminación",Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }
}
