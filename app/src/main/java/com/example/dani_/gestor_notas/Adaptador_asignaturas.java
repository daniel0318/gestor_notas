package com.example.dani_.gestor_notas;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Ramirez on 13/10/2016.
 */

/**
 * La finalidad de esta clase es crear desde cero nuestro adaptador para mostrarlo en el ListView
 * a nuestra manera y asi manejar más facilmente los datos en la lista, para esto se extiende
 * de BaseAdapter y se implementa sus metodos respectivos.
 */
public class Adaptador_asignaturas extends ArrayAdapter {

    private List lista= new ArrayList();

    public Adaptador_asignaturas(Context context, int resource) {
        super(context, resource);
    }

    class ContenedorView{
        TextView nombre_asignatura;
        TextView codigo_asignatura;
        TextView calificacion;
    }

    public void add(Asignatura object) {
        // TODO Auto-generated method stub
        lista.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View fila = convertView;
        ContenedorView contenedorView;
        if(convertView == null ){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fila = inflater.inflate(R.layout.view_asignaturas,parent,false);
            contenedorView = new ContenedorView();
            contenedorView.nombre_asignatura = (TextView) fila.findViewById(R.id.textView_nombre_asignatura);
            contenedorView.codigo_asignatura = (TextView) fila.findViewById(R.id.textView_codigo_asignatura);
            contenedorView.calificacion = (TextView) fila.findViewById(R.id.textView_nota_asignatura);
            fila.setTag(contenedorView);
        }else{
            contenedorView = (ContenedorView) fila.getTag();
        }

        Asignatura asignatura = (Asignatura) getItem(position);
        contenedorView.nombre_asignatura.setText(asignatura.getNombre());
        contenedorView.codigo_asignatura.setText(asignatura.getCodigo());
        contenedorView.calificacion.setText("Aun no :v");/**Falta aun :v*/

        return fila;
    }

}
