package com.example.dani_.gestor_notas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Agregar_nota extends AppCompatActivity {

    private ArrayList<String> tipos = new ArrayList<>();
    private Spinner spinner;
    private SpinnerAdapter spinnerAdapter = null;
    private TextView name;
    private EditText nombre;
    private EditText porcentaje;
    private EditText calificacion;
    private Button boton;
    private int posicion_spinner;
    private Asignatura asignatura;
    private Nota_general nota_general;/**Usada cuando voy a editar*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_nota);
        instanciar();
        oidos();
        llenar_spinner();
    }

    public void instanciar(){
        spinner = (Spinner) findViewById(R.id.spinner2);
        name = (TextView) findViewById(R.id.textView29);
        nombre = (EditText) findViewById(R.id.editText_Nombre_g);
        porcentaje = (EditText) findViewById(R.id.editText_porcentaje_g);
        calificacion = (EditText) findViewById(R.id.editText_nota_g);
        name.setVisibility(View.GONE);
        nombre.setVisibility(View.GONE);
        boton = (Button) findViewById(R.id.button_g);
        asignatura = (Asignatura) getIntent().getExtras().getSerializable("asignatura");

        try {/**Preguntamos si hay algo que nos interesa para saber si se va a editar*/
            if(getIntent().hasExtra("Editar")){
                boton.setText("Editar");
                asignatura = (Asignatura) getIntent().getExtras().getSerializable("asignatura");
                nota_general = (Nota_general) getIntent().getExtras().getSerializable("nota_general");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void oidos(){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String eleccion = tipos.get(position);
                if(eleccion.equals("Otros")){
                    name.setVisibility(View.VISIBLE);
                    nombre.setVisibility(View.VISIBLE);
                }
                posicion_spinner = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tipo = tipos.get(posicion_spinner);
                String nombre_g;
                if(tipo.equals("Otros")){
                    nombre_g = tipo;
                }else{
                    nombre_g = nombre.getText().toString();
                }

                int porcentaje_g = Integer.parseInt(porcentaje.getText().toString());
                double nota = Double.parseDouble(calificacion.getText().toString());
                String codigo = asignatura.getCodigo();

                ArrayList<Double> notas = new ArrayList<Double>();
                notas.add(nota);
                Nota_especifica nota_especifica = new Nota_especifica(nombre_g,notas);
                Nota_general nota_general = new Nota_general(nombre_g,porcentaje_g,nota);

            }
        });
    }

    public void llenar_spinner(){
        tipos.add("Parcial 1");
        tipos.add("Parcial 2");
        tipos.add("Parcial 3");
        tipos.add("Opcional 1");
        tipos.add("Opcional 2");
        tipos.add("Proyectos");
        tipos.add("Talleres");
        tipos.add("Quices");
        tipos.add("Otros");

        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tipos);
        spinner.setAdapter(spinnerAdapter);
    }

}
