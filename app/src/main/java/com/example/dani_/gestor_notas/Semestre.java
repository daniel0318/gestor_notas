package com.example.dani_.gestor_notas;

import java.io.Serializable;

/**
 * Created by DANIEL on 12/10/2016.
 */

public class Semestre implements Serializable{

    private int año = 0;
    private int semestre = 0;

    public Semestre(int año, int semestre) {
        this.año = año;
        this.semestre = semestre;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }
}
