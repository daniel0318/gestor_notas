package com.example.dani_.gestor_notas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Ramirez on 8/02/2016.
 */


/**
 * TODA CLASE VAYA A IMPLEMENTAR UNA  BASE DE DATOS DEBE EXTENDER DE SQLiteOpenHelper
 */

public class Base_de_datos extends SQLiteOpenHelper {

    /**
     * Constructor por defecto para SQLiteOpenHelper solo el contexto y se le pasa
     * al super el contexto, <el nombre que queremos para la BD>, cursos (en este caso null), y la version de la BD
     */
    public Base_de_datos(Context context) {
        super(context, "la_base_de_datos", null, 1);
    }


    /**
     * Oncreate se llamara solo si la base de datos no existe aun, si existe una base de datos no se llamara
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL("CREATE TABLE semestre( anio INTEGER, num_semestre INTEGER, PRIMARY KEY(num_semestre) ) ");
            System.out.println("*************1");
            db.execSQL("CREATE TABLE asignatura( num_semestre INTEGER, codigo TEXT, nombre TEXT, creditos INTEGER, PRIMARY KEY(codigo), FOREIGN KEY(num_semestre) REFERENCES semestre(num_semestre) ) ");
            System.out.println("*************2");
            db.execSQL("CREATE TABLE nota(nom_general TEXT, porcentaje INTEGER, calificacion_acumulada DECIMAL(1,3), codigo TEXT, PRIMARY KEY(nom_general), FOREIGN KEY(codigo) REFERENCES asignatura(codigo)  ) ");
            System.out.println("*************3");
            db.execSQL("CREATE TABLE notas(nombre TEXT, calificacion DECIMAL(1,3), nom_general TEXT, PRIMARY KEY(nom_general), FOREIGN KEY(nom_general) REFERENCES nota(nom_general) ) ");
            System.out.println("*************4");
        } catch (Exception e) {
            System.out.println("****** ERROR GENERANDO LAS TABLAS!!!! **********");
            e.printStackTrace();
        }

    }

    /**
     * Se llamara automaticamente si se detecta una nueva version de la base de datos
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //añadiendo semestres
    public void aniadir_semestre(int anio, int semestre) {

        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL("INSERT INTO semestre VALUES(" + anio + "," + semestre + ")");
            db.close();

        } catch (Exception e) {
            System.out.println("****Error al agregar el semestre****");
            e.printStackTrace();
        }


    }

    public List<String> leer_semestres(int tipo) {

        List<String> resultado = new ArrayList<String>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor;
            if(tipo==1){
                cursor = db.rawQuery("SELECT * FROM semestre", null, null);

                while (cursor.moveToNext()) {

                    resultado.add("Semestre " + cursor.getString(1) + " " + cursor.getString(0));
                }
            }else{
                cursor = db.rawQuery("SELECT num_semestre FROM semestre", null, null);

                while (cursor.moveToNext()) {

                    System.out.println("***********  " + cursor.getString(0));
                    resultado.add("Semestre " + cursor.getString(0));
                }
            }

            cursor.close();
            db.close();

        } catch (Exception e) {

            System.out.println("***** Error al leer los semestres ****");
            e.printStackTrace();

        }

        return resultado;
    }


    //Metodo para agregar datos
    public void ingresar_datos_asignatura(int num_semestre, String codigo, String nombre, int num_creditos) {

        SQLiteDatabase db = getWritableDatabase();
        try {
            System.out.println("*********" + num_semestre + ", " +
                    "'" + codigo + "', " +
                    "'" + nombre + "', " +
                    num_creditos + ",");

            db.execSQL("INSERT INTO asignatura VALUES(" +num_semestre + ", " +"'" + codigo + "', " +"'" + nombre + "', " +
                    num_creditos + ")");
            db.close();

            System.out.println("************inserción de asignatura exitosa************");
            ArrayList<Asignatura> lista = leer_asignaturas_semestre(new Semestre(2016,8));
            System.out.println("###############" + lista.size());

        } catch (Exception e) {
            System.out.println("************Error al insertar la asignatura************");
            e.printStackTrace();
        }

    }


    //metodo para leer los datos

    /**
     * Al leer de la base de datos nos devolvera un vector gerico
     * utilizando un objecto SQLiteDatabase usando la funcion de getReadableDatabase(), ya que solo leeremos
     * para ello hacemos uso de un Cursos y de rawQuery para poder pasarle una consulta generica
     * luego nos movemos por el cursos y se le añadimos los datos al vector que devolveremos
     */
    public ArrayList<Asignatura> leer_asignaturas_semestre(Semestre semestre) {

        System.out.println("#####Smestre es:" + semestre.getSemestre());
        ArrayList<Asignatura> resultado = new ArrayList<Asignatura>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT  * FROM asignatura WHERE num_semestre=" + semestre.getSemestre(), null, null);

            while (cursor.moveToNext()) {
                Asignatura asignatura = new Asignatura(cursor.getString(1), cursor.getString(2),
                        Integer.parseInt(cursor.getString(3)),Integer.parseInt(cursor.getString(0)));

                resultado.add(asignatura);

                System.out.println("***********  " + cursor.getString(0) + "****" + cursor.getString(1)
                        + cursor.getInt(2) + cursor.getInt(3));

            }


            cursor.close();
            db.close();
        } catch (Exception e) {
            System.out.println("******************Error al leer los datos: metodo leer_datos() *******");
            e.printStackTrace();
        }

        return resultado;
    }

    public void ingresar_nota(){

    }

}
