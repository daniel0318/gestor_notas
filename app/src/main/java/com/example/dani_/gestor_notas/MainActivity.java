package com.example.dani_.gestor_notas;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    ListView lista;
    FloatingActionButton boton;
    List<String> elementos_lista;
    Base_de_datos db;
    ArrayAdapter<String> adaptador;
    Semestre semestre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        definir_objectos();
        poner_oidos();
        llenar_lista();

    }

    //Definicion de objectos
    public void definir_objectos() {

        lista = (ListView) findViewById(R.id.listView);
        boton = (FloatingActionButton) findViewById(R.id.floatButton);
        db = new Base_de_datos(this);

        lista.setEmptyView(findViewById(R.id.empty));

        elementos_lista = new ArrayList<String>();  /**esta sera la lista que se le pasara al adapter para que muestre*/

        /**Asociamos el menu contextual*/
        registerForContextMenu(lista);

    }

    public void poner_oidos() {

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Bien", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), Add_semestre.class);
                startActivity(i);
            }
        });

        /**Metodo que sirve para traer el item seleccionado de la lista*/
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /**Dos formas de leer el dato seleccionado del listview ya sea por la lista o desde el adaptador*/

                String seleccionado = elementos_lista.get(position).toString();
                String [] texto = seleccionado.split(" ");
                semestre = new Semestre(Integer.parseInt(texto[2]), Integer.parseInt(texto[1]));
                Intent i = new Intent(getApplicationContext(), Lista_asignatura.class);
                i.putExtra("semestre", semestre);
                startActivity(i);


            }
        });

    }

    public void llenar_lista() {

        elementos_lista = db.leer_semestres(1);
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elementos_lista);
        lista.setAdapter(adaptador);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(Menu.NONE,1, Menu.NONE,"Editar");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE,2,Menu.NONE,"Eliminar").setHeaderTitle("Confirmar eliminacion");
        subMenu.add(Menu.NONE,21, Menu.NONE,"SI");
        subMenu.add(Menu.NONE,22, Menu.NONE,"NO");

        super.onCreateContextMenu(menu, v, menuInfo);


        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle(lista.getAdapter().getItem(info.position).toString());

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getItemId() == 1){
            ContextMenu.ContextMenuInfo menuInfo = item.getMenuInfo();
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

            String seleccionado = lista.getAdapter().getItem(info.position).toString();
            String [] texto = seleccionado.split(" ");
            semestre = new Semestre(Integer.parseInt(texto[2]), Integer.parseInt(texto[1]));

            Intent i = new Intent(getApplicationContext(), Add_semestre.class);
            i.putExtra("semestre", semestre);
            i.putExtra("Editar", "editando");
            startActivity(i);
        }
        else if(item.getItemId() == 21){
            Toast.makeText(getApplicationContext(),"NO SE QUE HACER :v",Toast.LENGTH_SHORT).show();
        }
        else if(item.getItemId() == 22){
            Toast.makeText(getApplicationContext(),"Se a cancelado la eliminación",Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }
}
