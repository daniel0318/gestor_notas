package com.example.dani_.gestor_notas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.Serializable;

public class Asignatura implements Serializable {

    private String codigo;
    private String nombre;
    private int creditos;
    private int num_semestre;

    public Asignatura(String codigo, String nombre, int creditos, int num_semestre) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.creditos = creditos;
        this.num_semestre = num_semestre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getNum_semestre() {
        return num_semestre;
    }

    public void setNum_semestre(int num_semestre) {
        this.num_semestre = num_semestre;
    }


}
