package com.example.dani_.gestor_notas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Add_semestre extends AppCompatActivity {

    TextView titulo; /**Este es el titulo*/
    EditText anio_semestre;
    EditText num_semestre;
    Button boton_agregar;
    ImageButton boton_regresar;
    Base_de_datos db;
    Semestre semestre; /**Lo usariamos para cuando vamos a editar*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_semestre);

        definir_objectos();
        colocar_oidos();

    }


    public void definir_objectos() {

        titulo = (TextView) findViewById(R.id.textView23);
        anio_semestre = (EditText) findViewById(R.id.editText3);
        num_semestre = (EditText) findViewById(R.id.editText4);
        boton_agregar = (Button) findViewById(R.id.button3);
        boton_regresar = (ImageButton) findViewById(R.id.imageButton5);
        db = new Base_de_datos(this); /**Es importante instanciar la base de datos o sino chilla el programa*/

        try {/**Preguntamos si hay algo que nos interesa para saber si se va a editar*/
            if(getIntent().hasExtra("Editar")){
                titulo.setText("Editar Semestre");
                boton_agregar.setText("Editar");
                semestre = (Semestre) getIntent().getExtras().getSerializable("semestre");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void colocar_oidos() {

        try {

            boton_agregar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(boton_agregar.getText().equals("Agregar")){
                    System.out.println("********"+getAnio_semestre()+"  "+ getNum_semestre());
                    db.aniadir_semestre(getAnio_semestre(), getNum_semestre());
                    Toast.makeText(getApplicationContext(),"se añadio correctamente...supongo", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);}


                }
            });

            boton_regresar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al insertar", Toast.LENGTH_SHORT).show();
            System.out.println("****Error en alguno de los listener****");
            e.printStackTrace();
        }

    }


    public int getAnio_semestre() {
        return Integer.parseInt(anio_semestre.getText().toString());
    }

    public int getNum_semestre() {
        return Integer.parseInt(num_semestre.getText().toString());
    }
}
