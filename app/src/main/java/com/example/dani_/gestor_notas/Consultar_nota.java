package com.example.dani_.gestor_notas;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Consultar_nota extends AppCompatActivity {

    TextView nombre_asignatura;
    ListView lista;
    FloatingActionButton boton;
    Asignatura asignatura;
    Nota_general nota_general;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_nota);
        instanciar();
        oidos();
    }

    public void instanciar(){
        asignatura = (Asignatura) getIntent().getExtras().getSerializable("asignatura");
        nombre_asignatura = (TextView) findViewById(R.id.textView10);
        nombre_asignatura.setText(asignatura.getNombre());
        lista = (ListView) findViewById(R.id.listView_nota_general);
        boton = (FloatingActionButton) findViewById(R.id.floatButton_nota);
        /**Asociamos el menu contextual*/
        registerForContextMenu(lista);
    }

    public void oidos(){
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Agregar_nota.class);
                intent.putExtra("asignatura",asignatura);
                startActivity(intent);
            }
        });
    }

    public void llenar_lista(){
        /**FALTA*/
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(Menu.NONE,1, Menu.NONE,"Editar");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE,2,Menu.NONE,"Eliminar").setHeaderTitle("Confirmar eliminacion");
        subMenu.add(Menu.NONE,21, Menu.NONE,"SI");
        subMenu.add(Menu.NONE,22, Menu.NONE,"NO");

        super.onCreateContextMenu(menu, v, menuInfo);


        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        nota_general = (Nota_general) lista.getAdapter().getItem(info.position);
        menu.setHeaderTitle(nota_general.getNombre());

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getItemId() == 1){

            Intent i = new Intent(getApplicationContext(), Add_semestre.class);
            i.putExtra("asignatura", asignatura);
            i.putExtra("nota_general",nota_general);
            i.putExtra("Editar", "editando");
            startActivity(i);
        }
        else if(item.getItemId() == 21){
            Toast.makeText(getApplicationContext(),"NO SE QUE HACER :v",Toast.LENGTH_SHORT).show();
        }
        else if(item.getItemId() == 22){
            Toast.makeText(getApplicationContext(),"Se a cancelado la eliminación",Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }
}
