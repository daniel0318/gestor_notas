package com.example.dani_.gestor_notas;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Ramirez on 16/10/2016.
 */

public class Adaptador_Consultar_nota extends ArrayAdapter {

    private List <Nota_general> lista = new ArrayList<>();

    public Adaptador_Consultar_nota(Context context, int resource) {
        super(context, resource);
    }

    class ContenedorView{
        TextView nombre;
        TextView porcentaje;
        TextView calificacion;
    }

    public void add(Nota_general object) {
        lista.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View fila = convertView;
        ContenedorView contenedorView;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fila = inflater.inflate(R.layout.view_nota_general,parent,false);
            contenedorView = new ContenedorView();
            contenedorView.nombre = (TextView) fila.findViewById(R.id.textView13);
            contenedorView.porcentaje = (TextView) fila.findViewById(R.id.textView14);
            contenedorView.calificacion = (TextView) fila.findViewById(R.id.textView15);
            fila.setTag(contenedorView);


        }else{
            contenedorView = (ContenedorView) fila.getTag();
        }

        Nota_general nota_general = (Nota_general) getItem(position);
        contenedorView.nombre.setText(nota_general.getNombre());
        contenedorView.porcentaje.setText(nota_general.getPorcentaje());
        contenedorView.calificacion.setText(""+nota_general.getCalificacion());


        return fila;
    }
}
