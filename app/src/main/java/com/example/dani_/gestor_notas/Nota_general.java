package com.example.dani_.gestor_notas;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Daniel Ramirez on 16/10/2016.
 * Esta clase representa la nota en general con respecto a por ejemplo un parcial o a los talleres
 */

public class Nota_general implements Serializable {

    private String nombre;
    private int porcentaje;
    private Double calificacion;
    private Nota_especifica nota_especifica;

    public Nota_general(String nombre, int porcentaje, Double calificacion) {
        this.nombre = nombre;
        this.porcentaje = porcentaje;
        this.calificacion = calificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Double calificacion) {
        this.calificacion = calificacion;
    }


}
