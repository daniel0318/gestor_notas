package com.example.dani_.gestor_notas;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Daniel Ramirez on 16/10/2016.
 * Esta clase representa una nota en especifica que es contenia por Nota_general.
 */

public class Nota_especifica implements Serializable {

    private String nombre;
    private ArrayList<Double> notas;

    public Nota_especifica(String nombre, ArrayList<Double> notas) {
        this.nombre = nombre;
        this.notas = notas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Double> getNotas() {
        return notas;
    }

    public void setNotas(ArrayList<Double> notas) {
        this.notas = notas;
    }
}
