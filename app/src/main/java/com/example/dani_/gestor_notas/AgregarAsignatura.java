package com.example.dani_.gestor_notas;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AgregarAsignatura extends AppCompatActivity {


    private TextView titulo;
    private ImageButton botonRegresa;
    private Button botonAgregar;
    private Base_de_datos db;
    private EditText cod_asignatura;
    private EditText nom_asignatura;
    private EditText num_creditos;
    private Spinner lista_semestres;
    private SpinnerAdapter adapter_semestre = null;
    private List<String> lista = null;
    private int semestre_seleccionado = 0;
    private Semestre semestre;
    private Asignatura asignatura; /**Se usaria cuando vamos a editar*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_asignatura);

        definir_objectos();
        llenar_lista_semestres();
        poder_oidos();

    }


    //sector para definir los botones
    public void definir_objectos() {
        titulo = (TextView) findViewById(R.id.textView2);
        semestre = (Semestre) getIntent().getExtras().getSerializable("semestre");
        botonRegresa = (ImageButton) findViewById(R.id.regresar_agregar_asignatura);
        botonAgregar = (Button) findViewById(R.id.Agregar__asignatura);
        cod_asignatura = (EditText) findViewById(R.id.editText);
        nom_asignatura = (EditText) findViewById(R.id.editText2);
        db = new Base_de_datos(this);
        lista_semestres = (Spinner) findViewById(R.id.spinner);
        num_creditos = (EditText) findViewById(R.id.editText5);

        try {/**Preguntamos si hay algo que nos interesa para saber si se va a editar*/
            if(getIntent().hasExtra("Editar")){
                titulo.setText("Editar asignatura");
                botonAgregar.setText("Editar");
                asignatura = (Asignatura) getIntent().getExtras().getSerializable("asignatura");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    //sector de oidos :v
    public void poder_oidos() {

        try {

            botonRegresa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), Lista_asignatura.class);
                    i.putExtra("semestre", semestre);
                    startActivity(i);
                }
            });


            lista_semestres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String []seleccionado =  lista.get(position).split(" ");
                    semestre_seleccionado = Integer.parseInt(seleccionado[1]);
                    Toast.makeText(getApplicationContext(), "##### " + semestre_seleccionado, Toast.LENGTH_LONG).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            botonAgregar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("desde el boton le mande: "+ semestre_seleccionado+ " " );
                    Toast.makeText(getApplicationContext(),""+semestre_seleccionado, Toast.LENGTH_LONG).show();
                    db.ingresar_datos_asignatura(semestre_seleccionado, getCod_asignatura(), getNom_asignatura(), getNum_creditos());

                    Intent i = new Intent(getApplicationContext(), Lista_asignatura.class);
                    i.putExtra("semestre", semestre);
                    startActivity(i);


                }
            });

        } catch (Exception e) {
            System.out.println("******* Problema al ponder los oidos de los botones y los Spinner! ayudalos estan sordos!! :c");
            e.printStackTrace();
        }

    }

    public void llenar_lista_semestres() {

        lista = new ArrayList<String>();
        lista = db.leer_semestres(2);
        System.out.println("*********************Va a entrar a inflar la lista del Spinner semestres**************");
        System.out.println(lista);
        adapter_semestre = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, lista);
        lista_semestres.setAdapter(adapter_semestre);

    }


    public String getNom_asignatura() {
        return nom_asignatura.getText().toString();
    }

    public String getCod_asignatura() {
        return cod_asignatura.getText().toString();
    }

    public Integer getNum_creditos() {
        return Integer.parseInt(num_creditos.getText().toString());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            Intent i = new Intent(getApplicationContext(), Lista_asignatura.class);
            i.putExtra("semestre", semestre);
            startActivity(i);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
